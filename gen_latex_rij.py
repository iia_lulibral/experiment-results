#!/usr/bin/python3

for x in [4,5,6,7,8]:
    for k in [4,5,6,7,8]:
        print("""
        \\begin{figure}
            \\centering
            \\input{tabs/rijXKopt.tex}
            \\caption{Rijndael ($x=X,k=K$) Step 1 opt results (time limit: 1h).}
            \\label{tab:rijXKopt}
        \\end{figure}
        \\begin{figure}
            \\centering
            \\input{tabs/rijXKenum.tex}
            \\caption{Rijndael ($x=X,k=K$) Step 1 enum results (time limit: 1h).}
            \\label{tab:rijXKenum}
        \\end{figure}
        \\clearpage
""".replace("X",str(x)).replace("K",str(k)))