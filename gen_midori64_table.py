#!/usr/bin/python3
from helper import read_json, extract_labels, extract_v




def main():
    ## midori64
    picat_dir = "21_03_05_midori64_picat/"
    chuffed_dir = "21_03_05_midori64_chuffed/"
    gurobi_dir = "21_03_05_midori64_gurobi/"

    rounds = list(range(3,17))
    file_prefix = "midori64_"


    # generate table with 0,0[v,picat,chuffed,gurobi] and 1,0[v,picat,chuffed,gurobi] for opt
    print("\\begin{tabular}{c||c|ccc||c|ccc}")
    print("& \\multicolumn{4}{|c|}{basic} & \\multicolumn{4}{|c}{advanced} \\\\")
    print("\\hline")
    print("r & v* & t picat & t chuffed & t gurobi & v* & t picat & t chuffed & t gurobi \\\\")
    print("\\hline")
    for r in rounds:
        # read data
        picat0   = read_json(picat_dir+    "{}{}_0_0.mzn".format(  file_prefix, r))
        picat1   = read_json(picat_dir+    "{}{}_1_0.mzn".format(  file_prefix, r))
        chuffed0 = read_json(chuffed_dir+  "{}{}_0_0.mzn".format(  file_prefix, r))
        chuffed1 = read_json(chuffed_dir+  "{}{}_1_0.mzn".format(  file_prefix, r))
        gurobi0  = read_json(gurobi_dir+   "{}{}_0_0_1.mzn".format(file_prefix, r))
        gurobi1  = read_json(gurobi_dir+   "{}{}_1_0_1.mzn".format(file_prefix, r))
        # create values
        v0 = extract_v([picat0,chuffed0,gurobi0], "v")
        v1 = extract_v([picat1,chuffed1,gurobi1], "v")
        n0 = extract_v([picat0,chuffed0,gurobi0], "nb_sols")
        n1 = extract_v([picat1,chuffed1,gurobi1], "nb_sols")
        pto0, pte0 = extract_labels(picat0, ["topt","tenum"])
        pto1, pte1 = extract_labels(picat1, ["topt","tenum"])
        cto0, cte0 = extract_labels(chuffed0, ["topt","tenum"])
        cto1, cte1 = extract_labels(chuffed1, ["topt","tenum"])
        gto0, gte0 = extract_labels(gurobi0, ["topt","tenum"])
        gto1, gte1 = extract_labels(gurobi1, ["topt","tenum"])
        print("{} & {} & {} & {} & {} & {} & {} & {} & {}   \\\\".format(
            r, v0, pto0, cto0, gto0, v1, pto1, cto1, gto1
        ))
    print("\\end{tabular}")

    # generate table with 0,0[v,picat,chuffed,gurobi] and 1,0[v,picat,chuffed,gurobi] for enum
    print("\\begin{tabular}{c||c|ccc||c|ccc}")
    print("& \\multicolumn{4}{|c|}{basic} & \\multicolumn{4}{|c}{advanced} \\\\")
    print("\\hline")
    print("r & nb sol & t picat & t chuffed & t gurobi & nb sol & t picat & t chuffed & t gurobi \\\\")
    print("\\hline")
    for r in rounds:
        # read data
        picat0   = read_json(picat_dir+    "{}{}_0_0.mzn".format(  file_prefix, r))
        picat1   = read_json(picat_dir+    "{}{}_1_0.mzn".format(  file_prefix, r))
        chuffed0 = read_json(chuffed_dir+  "{}{}_0_0.mzn".format(  file_prefix, r))
        chuffed1 = read_json(chuffed_dir+  "{}{}_1_0.mzn".format(  file_prefix, r))
        gurobi0  = read_json(gurobi_dir+   "{}{}_0_0_1.mzn".format(file_prefix, r))
        gurobi1  = read_json(gurobi_dir+   "{}{}_1_0_1.mzn".format(file_prefix, r))
        # create values
        v0 = extract_v([picat0,chuffed0,gurobi0], "v")
        v1 = extract_v([picat1,chuffed1,gurobi1], "v")
        n0 = extract_v([picat0,chuffed0,gurobi0], "nb_sols")
        n1 = extract_v([picat1,chuffed1,gurobi1], "nb_sols")
        pto0, pte0 = extract_labels(picat0, ["topt","tenum"])
        pto1, pte1 = extract_labels(picat1, ["topt","tenum"])
        cto0, cte0 = extract_labels(chuffed0, ["topt","tenum"])
        cto1, cte1 = extract_labels(chuffed1, ["topt","tenum"])
        gto0, gte0 = extract_labels(gurobi0, ["topt","tenum"])
        gto1, gte1 = extract_labels(gurobi1, ["topt","tenum"])
        print("{} & {} & {} & {} & {} & {} & {} & {} & {}   \\\\".format(
            r, n0, pte0, cte0, gte0, n1, pte1, cte1, gte1
        ))
    print("\\end{tabular}")


if __name__ == "__main__":
    main()