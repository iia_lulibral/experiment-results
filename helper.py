#!/usr/bin/python3
import json
from os import path

def read_json(filename, tlimit=3599):
    if path.exists(filename+".json"):
        with open(filename+".json") as f:
            d = json.load(f)
            return {
                "topt": d['topt'],
                "tenum": d['tenum'],
                "v": d['v'],
                "nb_sols": len(d['sols'])
            }
    elif path.exists(filename+".jsonopt.json"):
        with open(filename+".jsonopt.json") as f:
            d = json.load(f)
            if d['topt'] >= tlimit:
                return {}
            return {
                "topt": d['topt'],
                "v": d['v'],
            }
    else:
        return {}


def extract_v(json_list, label):
    for e in json_list:
        if label in e:
            return str(e[label])
    return "-"

def extract_label(e, label):
    if label in e:
        return str(int(e[label]))
    else:
        return "-"

def extract_labels(e, labels):
    return list(map(lambda a: extract_label(e,a), labels))



def main():
    # ## midori64
    # picat_dir = "21_03_05_midori64_picat/"
    # chuffed_dir = "21_03_05_midori64_chuffed/"
    # rounds = list(range(3,17))
    # file_prefix = "midori64_"
    # for r in rounds:
    #     print("== {} - {} ==".format(file_prefix, r))
    #     print(read_json(picat_dir+  "{}{}_0_0.mzn".format(file_prefix, r))) 
    #     print(read_json(picat_dir+  "{}{}_1_0.mzn".format(file_prefix, r)))
    #     print(read_json(chuffed_dir+"{}{}_0_0.mzn".format(file_prefix, r))) 
    #     print(read_json(chuffed_dir+"{}{}_1_0.mzn".format(file_prefix, r))) 


    # ## midori128
    # picat_dir = "21_03_05_midori128_picat/"
    # chuffed_dir = "21_03_05_midori128_chuffed/"
    # rounds = list(range(3,21))
    # file_prefix = "midori128_"
    # for r in rounds:
    #     print("== {} - {} ==".format(file_prefix, r))
    #     print(read_json(picat_dir+  "{}{}_0_0.mzn".format(file_prefix, r))) 
    #     print(read_json(picat_dir+  "{}{}_1_0.mzn".format(file_prefix, r)))
    #     print(read_json(chuffed_dir+"{}{}_0_0.mzn".format(file_prefix, r))) 
    #     print(read_json(chuffed_dir+"{}{}_1_0.mzn".format(file_prefix, r))) 


    ## rijndael
    picat_dir = "21_03_05_rijndael_picat/"
    chuffed_dir = "21_03_05_rijndael_chuffed/"
    x = 4
    k = 4
    rounds = list(range(3,14))
    file_prefix = "rijndael"
    par = "{}_{}".format(x,k)
    for r in rounds:
        print("== {} - {} ==".format(file_prefix, r))
        print(read_json(picat_dir+  "{}_{}_{}_0_0.mzn".format(file_prefix, r, par))) 
        print(read_json(picat_dir+  "{}_{}_{}_1_0.mzn".format(file_prefix, r, par)))
        print(read_json(chuffed_dir+"{}_{}_{}_0_0.mzn".format(file_prefix, r, par))) 
        print(read_json(chuffed_dir+"{}_{}_{}_1_0.mzn".format(file_prefix, r, par))) 



if __name__ == "__main__":
    main()