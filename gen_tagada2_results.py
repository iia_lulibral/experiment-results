from helper import read_json, extract_labels, extract_v

def display_field(d, n):
    if n in d:
        return "{:.0f}".format(d[n])
    else:
        return "--"


def generate_tab(file_prefix, rounds, variants, picat_dir, gurobi_dir):
    # tabular instructions
    tabular_instructions = "l"
    for _ in variants:
        tabular_instructions += "|llllll"
    print("\\begin{tabular}"+"{"+tabular_instructions+"}")
    nb_variants = len(variants)
    # display header
    variant_header = " "
    for e in variants:
        variant_header += " & \\multicolumn{6}{c}{"+e["n"]+"}"
    print(variant_header + "\\\\")
    header = "round"
    for _ in range(nb_variants):
        header += " &\t {} &\t {} &\t {} &\t {} &\t {} &\t {}".format(
            "v*", "T",
            "to P", "to G",
            "te P", "te G"    
        )
    print(header + "\\\\")
    # display content
    for r in rounds:
        picat_variants = list(map(
            lambda e: read_json(picat_dir+"{}{}_{}.json.mzn".format(file_prefix, r, e["f"])),
            variants
        ))
        gurobi_variants = list(map(
            lambda e: read_json(gurobi_dir+"{}{}_{}.json.mzn".format(file_prefix, r, e["f"])),
            variants
        ))
        v_variants = [extract_v([picat_variants[i], gurobi_variants[i]], "v") for i in range(nb_variants)]
        n_variants = [extract_v([picat_variants[i], gurobi_variants[i]], "nb_sols") for i in range(nb_variants)]
        line = "{}".format(r)
        for i in range(nb_variants):
            line += " &\t {} &\t {} &\t {} &\t {} &\t {} &\t {}".format(
                v_variants[i], n_variants[i],
                display_field(picat_variants[i], "topt"),  display_field(gurobi_variants[i], "topt"),
                display_field(picat_variants[i], "tenum"), display_field(gurobi_variants[i], "tenum")
            )
        print(line + "\\\\")
    print("\\end{tabular}")

        

def gen_midori64():
    picat_dir = "21_04_23_tagada2_picat/"
    gurobi_dir = "21_04_25_tagada2_gurobi/"
    rounds = list(range(3,16+1))
    file_prefix = "midori_64_"
    variants = [
        {"f":"0_0_0", "n":"$S_x=0$"},
        # {"f":"1_0_0", "n":"$S_x=1$"},
        # {"f":"2_0_0", "n":"$S_x=2$"},
        {"f":"3_0_0", "n":"$S_x=3$"},
        {"f":"4_0_0", "n":"$S_x=4$"},
        # {"f":"0_1_0", "n":"$S_x=0, diff$"},
        # {"f":"1_1_0", "n":"$S_x=1, diff$"},
        # {"f":"2_1_0", "n":"$S_x=2, diff$"},
        # {"f":"3_1_0", "n":"$S_x=3, diff$"},
        # {"f":"4_1_0", "n":"$S_x=4, diff$"},
    ]
    generate_tab(file_prefix, rounds, variants, picat_dir, gurobi_dir)
    variants = [
        # {"f":"0_0_0", "n":"$S_x=0$"},
        # {"f":"1_0_0", "n":"$S_x=1$"},
        # {"f":"2_0_0", "n":"$S_x=2$"},
        # {"f":"3_0_0", "n":"$S_x=3$"},
        # {"f":"4_0_0", "n":"$S_x=4$"},
        {"f":"0_1_0", "n":"$S_x=0, diff$"},
        # {"f":"1_1_0", "n":"$S_x=1, diff$"},
        # {"f":"2_1_0", "n":"$S_x=2, diff$"},
        {"f":"3_1_0", "n":"$S_x=3, diff$"},
        {"f":"4_1_0", "n":"$S_x=4, diff$"},
    ]
    generate_tab(file_prefix, rounds, variants, picat_dir, gurobi_dir)


def gen_midori128():
    picat_dir = "21_04_23_tagada2_picat/"
    gurobi_dir = "21_04_25_tagada2_gurobi/"
    rounds = list(range(3,20+1))
    file_prefix = "midori_128_"
    variants = [
        {"f":"0_0_0", "n":"$S_x=0$"},
        # {"f":"1_0_0", "n":"$S_x=1$"},
        # {"f":"2_0_0", "n":"$S_x=2$"},
        {"f":"3_0_0", "n":"$S_x=3$"},
        {"f":"4_0_0", "n":"$S_x=4$"},
        # {"f":"0_1_0", "n":"$S_x=0, diff$"},
        # {"f":"1_1_0", "n":"$S_x=1, diff$"},
        # {"f":"2_1_0", "n":"$S_x=2, diff$"},
        # {"f":"3_1_0", "n":"$S_x=3, diff$"},
        # {"f":"4_1_0", "n":"$S_x=4, diff$"},
    ]
    generate_tab(file_prefix, rounds, variants, picat_dir, gurobi_dir)
    print()
    print()
    print()
    variants = [
        # {"f":"0_0_0", "n":"$S_x=0$"},
        # {"f":"1_0_0", "n":"$S_x=1$"},
        # {"f":"2_0_0", "n":"$S_x=2$"},
        # {"f":"3_0_0", "n":"$S_x=3$"},
        # {"f":"4_0_0", "n":"$S_x=4$"},
        {"f":"0_1_0", "n":"$S_x=0, diff$"},
        # {"f":"1_1_0", "n":"$S_x=1, diff$"},
        # {"f":"2_1_0", "n":"$S_x=2, diff$"},
        {"f":"3_1_0", "n":"$S_x=3, diff$"},
        {"f":"4_1_0", "n":"$S_x=4, diff$"},
    ]
    generate_tab(file_prefix, rounds, variants, picat_dir, gurobi_dir)


def gen_aes128():
    picat_dir = "21_04_23_tagada2_picat/"
    gurobi_dir = "21_04_25_tagada2_gurobi/"
    rounds = list(range(3,5+1))
    file_prefix = "rijndael_4_4_"
    variants = [
        {"f":"0_0_0", "n":"$S_x=0$"},
        {"f":"4_0_0", "n":"$S_x=4$"},
        {"f":"5_0_0", "n":"$S_x=5$"},
    ]
    generate_tab(file_prefix, rounds, variants, picat_dir, gurobi_dir)
    print()
    print()
    print()
    variants = [
        {"f":"0_1_0", "n":"$S_x=0$ diff"},
        {"f":"4_1_0", "n":"$S_x=4$ diff"},
        {"f":"5_1_0", "n":"$S_x=5$ diff"},
    ]
    generate_tab(file_prefix, rounds, variants, picat_dir, gurobi_dir)
    print()
    print()
    print()
    variants = [
        {"f":"0_1_1", "n":"$S_x=0$ diff tr"},
        {"f":"4_1_1", "n":"$S_x=4$ diff tr"},
        {"f":"5_1_1", "n":"$S_x=5$ diff tr"},
    ]
    generate_tab(file_prefix, rounds, variants, picat_dir, gurobi_dir)



def gen_aes192():
    picat_dir = "21_04_23_tagada2_picat/"
    gurobi_dir = "21_04_25_tagada2_gurobi/"
    rounds = list(range(3,10+1))
    file_prefix = "rijndael_4_6_"
    variants = [
        {"f":"0_0_0", "n":"$S_x=0$"},
        {"f":"4_0_0", "n":"$S_x=4$"},
        {"f":"5_0_0", "n":"$S_x=5$"},
    ]
    generate_tab(file_prefix, rounds, variants, picat_dir, gurobi_dir)
    print()
    print()
    print()
    variants = [
        {"f":"0_1_0", "n":"$S_x=0$ diff"},
        {"f":"4_1_0", "n":"$S_x=4$ diff"},
        {"f":"5_1_0", "n":"$S_x=5$ diff"},
    ]
    generate_tab(file_prefix, rounds, variants, picat_dir, gurobi_dir)
    print()
    print()
    print()
    variants = [
        {"f":"0_1_1", "n":"$S_x=0$ diff tr"},
        {"f":"4_1_1", "n":"$S_x=4$ diff tr"},
        {"f":"5_1_1", "n":"$S_x=5$ diff tr"},
    ]
    generate_tab(file_prefix, rounds, variants, picat_dir, gurobi_dir)



def gen_aes256():
    picat_dir = "21_04_23_tagada2_picat/"
    gurobi_dir = "21_04_25_tagada2_gurobi/"
    rounds = list(range(3,14+1))
    file_prefix = "rijndael_4_8_"
    variants = [
        {"f":"0_0_0", "n":"$S_x=0$"},
        {"f":"4_0_0", "n":"$S_x=4$"},
        {"f":"5_0_0", "n":"$S_x=5$"},
    ]
    generate_tab(file_prefix, rounds, variants, picat_dir, gurobi_dir)
    print()
    print()
    print()
    variants = [
        {"f":"0_1_0", "n":"$S_x=0$ diff"},
        {"f":"4_1_0", "n":"$S_x=4$ diff"},
        {"f":"5_1_0", "n":"$S_x=5$ diff"},
    ]
    generate_tab(file_prefix, rounds, variants, picat_dir, gurobi_dir)
    print()
    print()
    print()
    variants = [
        {"f":"0_1_1", "n":"$S_x=0$ diff tr"},
        {"f":"4_1_1", "n":"$S_x=4$ diff tr"},
        {"f":"5_1_1", "n":"$S_x=5$ diff tr"},
    ]
    generate_tab(file_prefix, rounds, variants, picat_dir, gurobi_dir)

def gen_skinny_0():
    picat_dir = "21_04_23_tagada2_picat/"
    gurobi_dir = "21_04_25_tagada2_gurobi/"
    rounds = list(range(3,23+1))
    file_prefix = "skinny_64_0_"
    variants = [
        {"f":"0_0_0", "n":"$S_x=0$"},
        {"f":"3_0_0", "n":"$S_x=3$"},
        {"f":"4_0_0", "n":"$S_x=4$"},
    ]
    generate_tab(file_prefix, rounds, variants, picat_dir, gurobi_dir)


def gen_skinny_1():
    picat_dir = "21_04_23_tagada2_picat/"
    gurobi_dir = "21_04_25_tagada2_gurobi/"
    rounds = list(range(3,23+1))
    file_prefix = "skinny_64_1_"
    variants = [
        {"f":"0_0_0", "n":"$S_x=0$"},
        {"f":"3_0_0", "n":"$S_x=3$"},
        {"f":"4_0_0", "n":"$S_x=4$"},
    ]
    generate_tab(file_prefix, rounds, variants, picat_dir, gurobi_dir)


if __name__ == "__main__":
    gen_skinny_1()
