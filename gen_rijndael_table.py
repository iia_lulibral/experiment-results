#!/usr/bin/python3
from helper import read_json, extract_labels, extract_v
from sys import argv

rijndael_rounds = {
    (4,4): 6,
    (4,5): 8,
    (4,6): 10,
    (4,7): 12,
    (4,8): 14,
    (5,4): 4,
    (5,5): 6,
    (5,6): 9,
    (5,7): 10,
    (5,8): 12,
    (6,4): 3,
    (6,5): 5,
    (6,6): 7,
    (6,7): 9,
    (6,8): 10,
    (7,4): 3,
    (7,5): 4,
    (7,6): 6,
    (7,7): 7,
    (7,8): 9,
    (8,4): 3,
    (8,5): 4,
    (8,6): 5,
    (8,7): 6,
    (8,8): 8
}


def main(x,k,t):
    picat_dir =   "21_03_05_rijndael_picat/"
    chuffed_dir = "21_03_15_rijndael_chuffed/"
    gurobi_dir = "21_03_15_rijndael_gurobi/"
    par = "{}_{}".format(x,k)
    rounds = list(range(3,rijndael_rounds[(x,k)]+1))
    file_prefix = "rijndael"

    if t == "opt":
        print()
        print()
        print()
        # generate table with 0,0[v,picat,chuffed,gurobi] and 1,0[v,picat,chuffed,gurobi] for opt & enum
        print("\\begin{tabular}{c||c|ccc||c|ccc||c|ccc}")
        print("& \\multicolumn{4}{|c|}{basic} & \\multicolumn{4}{|c|}{advanced} & \\multicolumn{4}{|c}{advanced+transitivity} \\\\")
        print("\\hline")
        print("r & v* & picat & chuffed & gurobi & v* & picat & chuffed & gurobi & v* & picat & chuffed & gurobi \\\\")
        print("\\hline")
        for r in rounds:
            # read data
            picat0   = read_json(picat_dir+    "{}_{}_{}_0_0.mzn".format(  file_prefix, r, par))
            picat1   = read_json(picat_dir+    "{}_{}_{}_1_0.mzn".format(  file_prefix, r, par))
            picat2   = read_json(picat_dir+    "{}_{}_{}_1_1.mzn".format(  file_prefix, r, par))
            chuffed0 = read_json(chuffed_dir+  "{}_{}_{}_0_0_0.mzn".format(  file_prefix, r, par))
            chuffed1 = read_json(chuffed_dir+  "{}_{}_{}_1_0_0.mzn".format(  file_prefix, r, par))
            chuffed2 = read_json(chuffed_dir+  "{}_{}_{}_1_1_0.mzn".format(  file_prefix, r, par))
            gurobi0  = read_json(gurobi_dir+   "{}_{}_{}_0_0_1.mzn".format(file_prefix, r, par))
            gurobi1  = read_json(gurobi_dir+   "{}_{}_{}_1_0_1.mzn".format(file_prefix, r, par))
            gurobi2  = read_json(gurobi_dir+   "{}_{}_{}_1_1_1.mzn".format(file_prefix, r, par))
            # create values
            v0 = extract_v([picat0,chuffed0,gurobi0], "v")
            v1 = extract_v([picat1,chuffed1,gurobi1], "v")
            v2 = extract_v([picat2,chuffed2,gurobi2], "v")
            n0 = extract_v([picat0,chuffed0,gurobi0], "nb_sols")
            n1 = extract_v([picat1,chuffed1,gurobi1], "nb_sols")
            n2 = extract_v([picat2,chuffed2,gurobi2], "nb_sols")
            pto0, pte0 = extract_labels(picat0, ["topt","tenum"])
            pto1, pte1 = extract_labels(picat1, ["topt","tenum"])
            pto2, pte2 = extract_labels(picat2, ["topt","tenum"])
            cto0, cte0 = extract_labels(chuffed0, ["topt","tenum"])
            cto1, cte1 = extract_labels(chuffed1, ["topt","tenum"])
            cto2, cte2 = extract_labels(chuffed2, ["topt","tenum"])
            gto0, gte0 = extract_labels(gurobi0, ["topt","tenum"])
            gto1, gte1 = extract_labels(gurobi1, ["topt","tenum"])
            gto2, gte2 = extract_labels(gurobi2, ["topt","tenum"])
            print("{} & {} & {} & {} & {} & {} & {} & {} & {} & {} & {} & {} & {}  \\\\".format(
                r, v0, pto0, cto0, gto0, v1, pto1, cto1, gto1, v2, pto2, cto2, gto2
            ))
            # print("{} & {} & {} & {} & {} & {} & {} & {} & {}  & {} & {} & {} & {} & {} & {} & {} & {} \\\\".format(
            #     r, v0, pto0, cto0, gto0, v1, pto1, cto1, gto1,   n0, pte0, cte0, gte0, n1, pte1, cte1, gte1
            # ))
        print("\\end{tabular}")
    else:
        print()
        print()
        print()
            # generate table with 0,0[v,picat,chuffed,gurobi] and 1,0[v,picat,chuffed,gurobi] for opt & enum
        print("\\begin{tabular}{c||c|ccc||c|ccc||c|ccc}")
        print("& \\multicolumn{4}{|c|}{basic} & \\multicolumn{4}{|c|}{advanced} & \\multicolumn{4}{|c}{advanced+transitivity} \\\\")
        print("\\hline")
        print("r & nb sol & picat & chuffed & gurobi & nb sol & picat & chuffed & gurobi & nb sol & picat & chuffed & gurobi \\\\")
        print("\\hline")
        for r in rounds:
            # read data
            picat0   = read_json(picat_dir+    "{}_{}_{}_0_0.mzn".format(  file_prefix, r, par))
            picat1   = read_json(picat_dir+    "{}_{}_{}_1_0.mzn".format(  file_prefix, r, par))
            picat2   = read_json(picat_dir+    "{}_{}_{}_1_1.mzn".format(  file_prefix, r, par))
            chuffed0 = read_json(chuffed_dir+  "{}_{}_{}_0_0.mzn".format(  file_prefix, r, par))
            chuffed1 = read_json(chuffed_dir+  "{}_{}_{}_1_0.mzn".format(  file_prefix, r, par))
            chuffed2 = read_json(chuffed_dir+  "{}_{}_{}_1_1.mzn".format(  file_prefix, r, par))
            gurobi0  = read_json(gurobi_dir+   "{}_{}_{}_0_0_1.mzn".format(file_prefix, r, par))
            gurobi1  = read_json(gurobi_dir+   "{}_{}_{}_1_0_1.mzn".format(file_prefix, r, par))
            gurobi2  = read_json(gurobi_dir+   "{}_{}_{}_1_1_1.mzn".format(file_prefix, r, par))
            # create values
            v0 = extract_v([picat0,chuffed0,gurobi0], "v")
            v1 = extract_v([picat1,chuffed1,gurobi1], "v")
            v2 = extract_v([picat2,chuffed2,gurobi2], "v")
            n0 = extract_v([picat0,chuffed0,gurobi0], "nb_sols")
            n1 = extract_v([picat1,chuffed1,gurobi1], "nb_sols")
            n2 = extract_v([picat2,chuffed2,gurobi2], "nb_sols")
            pto0, pte0 = extract_labels(picat0, ["topt","tenum"])
            pto1, pte1 = extract_labels(picat1, ["topt","tenum"])
            pto2, pte2 = extract_labels(picat2, ["topt","tenum"])
            cto0, cte0 = extract_labels(chuffed0, ["topt","tenum"])
            cto1, cte1 = extract_labels(chuffed1, ["topt","tenum"])
            cto2, cte2 = extract_labels(chuffed2, ["topt","tenum"])
            gto0, gte0 = extract_labels(gurobi0, ["topt","tenum"])
            gto1, gte1 = extract_labels(gurobi1, ["topt","tenum"])
            gto2, gte2 = extract_labels(gurobi2, ["topt","tenum"])
            print("{} & {} & {} & {} & {} & {} & {} & {} & {} & {} & {} & {} & {}  \\\\".format(
                r, n0, pte0, cte0, gte0, n1, pte1, cte1, gte1, n2, pte2, cte2, gte2
            ))
            # print("{} & {} & {} & {} & {} & {} & {} & {} & {}  & {} & {} & {} & {} & {} & {} & {} & {} \\\\".format(
            #     r, v0, pto0, cto0, gto0, v1, pto1, cto1, gto1,   n0, pte0, cte0, gte0, n1, pte1, cte1, gte1
            # ))
        print("\\end{tabular}")


if __name__ == "__main__":
    main(int(argv[1]), int(argv[2]), argv[3])